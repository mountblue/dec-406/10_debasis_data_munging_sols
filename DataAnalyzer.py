class DataAnalyzer:
    """Takes a function with given dictionary dataset as parameter
    calculates minimum value of two column data
    from the given dataset with key as column 1 data
    and value as list of two other column data
    """

    def __init__(self, original_func):
        self.original_func = original_func

    def __call__(self, *args, **kwargs):
        data_object = self.original_func(*args, **kwargs)
        return sorted(
            DataAnalyzer.calculate_min_value(data_object).items(),
            key=lambda x: x[1]
        )[0]

    @staticmethod
    def calculate_min_value(dictionary):
        min_pair = {}
        for key, value in dictionary.items():
            min_pair[key] = abs(int(value[0]) - int(value[1]))
        return min_pair


@DataAnalyzer
def get_min_data(datas):
    return datas
