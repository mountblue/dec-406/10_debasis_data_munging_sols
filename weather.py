from DataAnalyzer import get_min_data
from DatFileParser import DatFileParser


# Get minimum diffrence from weather.dat file

weatherdata = DatFileParser().parse_file("./data/weather.dat", 0, 1, 2)
print(get_min_data(weatherdata))
