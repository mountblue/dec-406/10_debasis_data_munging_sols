import re


class DatFileParser:
    """ Parses a .dat file and extracts data """

    def __init__(self):
        self.data_holder = []

    def parse_file(self, file_path, col_one, col_two, col_three):
        """Takes a .dat file from the specified path
        Does I/O opeations
        To read and extract data from the file for sepcified columns
        """
        with open(file_path, "r") as file:
            for line in file.readlines()[1:]:
                if line != "\n":
                    data = filter(
                        lambda x: x != "" and x != "\n", line.split(" ")
                    )
                    self.data_holder.append(list(data))
        filtered_data = [data for data in self.data_holder if len(data) > 1]
        return DatFileParser.organize_data(col_one,
                                           col_two,
                                           col_three,
                                           filtered_data)

    @staticmethod
    def organize_data(col_one, col_two, col_three, list_of_data):
        data_dictionary = {}
        pattern = re.compile("\d+")
        for row_data in list_of_data:
            data_dictionary[row_data[col_one]] = [
                re.match(pattern, row_data[col_two]).group(),
                re.match(pattern, row_data[col_three]).group()
            ]
        return data_dictionary
