from DataAnalyzer import get_min_data
from DatFileParser import DatFileParser

# Get minimun diffrence from football.dat file

footballdata = DatFileParser().parse_file("./data/football.dat", 1, 6, 8)
print(get_min_data(footballdata))
